﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Prometheus;
using MetricsGenerator.Models;

namespace MetricsGenerator.Controllers
{
    [Route("api/[controller]")]
    //[ApiController]
    public class MetricsGeneratorController : ControllerBase
    {
        [HttpGet]
        public string Index()
        {
            return "SEE https://github.com/prometheus-net/prometheus-net";
        }

        [HttpPost("setup")]
        public IActionResult Setup([FromBody]GaugeSetupDto gauge) {
           var m = Metrics.CreateGauge(gauge.Name, null, new GaugeConfiguration {LabelNames = gauge.Labels});
           m.Publish();
           return Ok();
        }

      [HttpPost("gauge")]
        public IActionResult Gauge([FromBody]GaugeValueDto gauge)
        {
         //this is not optimal, but ok for demo purposes
            var m = Metrics.CreateGauge(gauge.Name, null, new GaugeConfiguration
            {
               LabelNames = gauge.Labels.Select(l=> l.Key).ToArray()
            }).WithLabels(gauge.Labels.Select(l => l.Value).ToArray());
            m.Set(gauge.Value);
            m.Publish();            
            return Ok();
        }
    }
}
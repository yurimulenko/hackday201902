﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MetricsGenerator.Models {
   public class GaugeSetupDto {
      public string Name { get; set; }
      public string[] Labels { get; set; }
   }
}

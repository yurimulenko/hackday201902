﻿using System;
using System.Collections.Generic;

namespace MetricsGenerator.Models
{
    /// <summary>
    /// Wrapper for <see cref="Prometheus.Gauge"/>
    /// </summary>
    public class GaugeValueDto
    {
        public string Name { get; set; }
        public double Value { get; set; }
        public KeyValuePair<string, string>[] Labels { get; set; }
    }
}
